<div class="follow">
    <a class="follow-twitter text-dark" href="https://twitter.com/historiachiqui" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-twitter fa-stack-1x fa-1x fa-inverse"></i>
        </span>
    </a>

    <a class="follow-facebook text-dark" href="https://www.facebook.com/historiachiquita/" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-facebook-official fa-stack-1x fa-1x fa-inverse"></i>
        </span>
    </a>

    <a class="follow-instagram text-dark" href="https://www.instagram.com/historiachiquita/" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-instagram fa-stack-1x fa-1x fa-inverse"></i>
        </span>
    </a>

    <a class="follow-vimeo text-dark" href="https://vimeo.com/user117738643" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-vimeo fa-stack-1x fa-1x fa-inverse"></i>
        </span>
    </a>
</div>
