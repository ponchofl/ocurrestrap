<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div class="bg-white" id="wrapper-navbar">
		<div class="container-fluid sticky-top px-0" itemscope itemtype="http://schema.org/WebSite">
		
		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

			<div class="row no-gutters align-items-center py-0 border-bottom">

				<div class="col-2 d-md-none border-right d-flex justify-content-center">
					<button class="py-3 bg-transparent border-0" data-toggle="collapse" data-target="#navbarNavDropdown" onclick="openNav()">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div class="col-sm-2 col-8 d-flex justify-content-center">
					<!-- Branding call -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand">
								<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>
							</h1>

						<?php else : ?>

							<a class="navbar-brand w-75" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo('full', array( 'class' => 'logotipo' ));
					} ?><!-- end custom logo -->
				</div>

				<nav class="col-sm-8 col navbar navbar-expand-md p-0 justify-content-center d-none d-md-block">

					<h2 id="main-nav-label" class="sr-only">
						<?php esc_html_e( 'Main Navigation', 'understrap' ); ?>
					</h2>

					<!-- Menú de Wordpress -->
					<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => 'collapse navbar-collapse h-100',
								'container_id'    => 'navbar',
								'menu_class'      => 'navbar-nav m-auto',
								'fallback_cb'     => '',
								'menu_id'         => 'main-menu',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						);
						?>

				</nav><!-- .site-navigation -->

				<div class="col-sm-2 col m-auto d-none d-lg-block"> <!--Ícono de buscar hasta md-->
					<a class="btn" data-toggle="modal" data-target="#exampleModalCenter">
						<button class="btn px-0 py-0"><i class="fa fa-search"></i>&nbsp; Buscar</button>
					</a>
				</div>

				<div class="col-sm-2 col m-auto d-lg-none text-center"> <!--Ícono de buscar después de md-->
					<a class="btn" data-toggle="modal" data-target="#exampleModalCenter">
						<button class="btn px-0 pb-0"><i class="fa fa-search"></i></button>
					</a>
				</div>

			</div> <!-- .row -->
		</div> <!-- container-fluid -->
	</div><!-- #wrapper-navbar end -->

	<!-- ******************* Módulo flotante de buscador ******************* -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header border-0 pb-1">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body pb-5">
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- ******************* Módulo flotante de buscador ******************* -->

	<!-- ******************* Menú desplegable lateral ******************* -->
	<div id="navbarNavDropdown" class="navbar-collapse collapse bg-light shadow-lg container">
		
		<div class="row py-2 m-0 align-items-center justify-content-end">
			<div class="col-6">
				<?php if ( ! has_custom_logo() ) { ?>

				<?php if ( is_front_page() && is_home() ) : ?>

					<h1 class="navbar-brand">
						<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>
					</h1>

				<?php else : ?>

					<a class="navbar-brand w-75" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

				<?php endif; ?>


				<?php } else {
				the_custom_logo('full', array( 'class' => 'logotipo' ));
				} ?><!-- end custom logo -->
			</div>
			<div class="col-6 text-right">
				<a class="btn closebtn p-0" data-toggle="collapse" data-target="#navbarNavDropdown" onclick="closeNav()">
					<button class="btn px-0 pb-0"><i class="fa fa-times"></i>&nbsp;</button>
				</a>
			</div>
		</div> <!-- .row -->

		<div class="row">
			<div class="col p-0 border-top">
				<?php
					wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'menu_class'      => 'navbar-nav m-auto',
							'fallback_cb'     => '',
							'menu_id'         => 'main-menu',
							'depth'           => 2,
							'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
						)
					);
				?>
			</div>
		</div> <!-- .row -->

		<div class="row">
			<div class="col">
				<?php wp_follow_botones(); ?>
			</div>
		</div>

	</div>
	<!-- ******************* Menú Toggler ******************* -->
