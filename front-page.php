<?php
/**
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

    <div class="wrapper pt-0" id="frontpage-wrapper">
        
        <div class="container">

            <div class="row align-items-center" id="hero">
                <div class="col-12 py-5">
                    <?php dynamic_sidebar( 'hero' ); ?>
                </div>
            </div>

            <h1 class="secciones">Sección con query y loop</h1>
            
            <div class="row no-gutters card-deck pt-4" id="primera-seccion">
                
                <div class="col-sm-8 col-12 bg-orange card px-sm-0">
                        
                    <?php
                        $args = array(
                        'post_type' => 'post',
                        'category_name' => 'ultima',
                        'orderby'       => 'date',
                        'order'         => 'DESC',
                        'showposts' => 1,
                        );

                        $wp_proyectos = new WP_Query($args);

                        if($wp_proyectos->have_posts()) :
                        while($wp_proyectos->have_posts()) :
                        $wp_proyectos->the_post();
                    ?>

                        <div class="row g-0 text-white h-100">
                            <div class="col-md-5 pr-sm-0">
                                <a href="<?php the_permalink() ?>">
                                    <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('full', array( 'class' => 'd-block mx-auto' ));
                                    } else { ?>
                                    <svg class="bg-light w-100">
                                        <rect/>
                                    </svg>
                                    <?php } ?>
                                </a>
                            </div>
                            <div class="col-md-7 pl-sm-0">
                            <div class="card-body text-white">
                                <h1 class="card-title"><a href='<?php the_permalink(); ?>' class="text-white"><?php the_title(); ?></a></h1>
                                <?php the_excerpt(); ?>
                            </div>
                            </div>
                        </div>

                    <?php endwhile; endif; wp_reset_query(); ?>

                </div> <!--.col-->

                <div class="col-sm-4 col-12 bg-purple card px-0" id="segunda-seccion">
                        
                    <?php
                        $args = array(
                        'post_type' => 'post',
                        'category_name' => 'sabado-comun-denominadores',
                        'orderby'       => 'date',
                        'order'         => 'DESC',
                        'showposts' => 1,
                        );

                        $wp_proyectos = new WP_Query($args);

                        if($wp_proyectos->have_posts()) :
                        while($wp_proyectos->have_posts()) :
                        $wp_proyectos->the_post();
                    ?>

                            <a href="<?php the_permalink() ?>">
                                <?php if ( has_post_thumbnail() ) {
                                the_post_thumbnail('full', array( 'class' => 'd-block w-100' ));
                                } else { ?>
                                <svg class="bg-light w-100">
                                    <rect/>
                                </svg>
                                <?php } ?>
                            </a>
                            <div class="card-body text-white">
                                <h1 class="card-title"><a href='<?php the_permalink(); ?>' class="text-white"><?php the_title(); ?></a></h1>
                                <?php the_excerpt(); ?>
                            </div>

                    <?php endwhile; endif; wp_reset_query(); ?>

                </div> <!--.col-->

            </div><!--.row-->

            <div class="row no-gutters pt-sm-4" id="actividades">

                <div class="col-12 card px-0">

                    <?php
                        $args = array(
                        'post_type' => 'post',
                        'category_name' => 'actividades',
                        'orderby'       => 'date',
                        'order'         => 'DESC',
                        'showposts' => 1,
                        );

                        $wp_proyectos = new WP_Query($args);

                        if($wp_proyectos->have_posts()) :
                        while($wp_proyectos->have_posts()) :
                        $wp_proyectos->the_post();
                    ?>

                        <a href="<?php the_permalink() ?>">
                            <?php if ( has_post_thumbnail() ) {
                            the_post_thumbnail('full', array( 'class' => 'd-block w-100' ));
                            } else { ?>
                            <svg class="bg-light w-100">
                                <rect/>
                            </svg>
                            <?php } ?>
                        </a>
                        <div class="card-img-overlay rounded-0 d-flex">
                            <h1 class="text-center m-auto"><a href='<?php the_permalink(); ?>' class="text-white"><?php the_title(); ?></a></h1>
                        </div>

                    <?php endwhile; endif; wp_reset_query(); ?>

                    
                </div>

            </div><!--.row-->

            
            <div class="row pt-5" id="materiales">

                <div class="col-12">

                    <h1 class="secciones">Materiales</h1>

                    <div class="card-deck pt-4">

                        <?php
                            $args = array(
                            'post_type' => 'post',
                            'category_name' => 'materiales-destacados',
                            'orderby'       => 'date',
                            'order'         => 'DESC',
                            'showposts' => 3,
                            );

                            $wp_proyectos = new WP_Query($args);

                            if($wp_proyectos->have_posts()) :
                            while($wp_proyectos->have_posts()) :
                            $wp_proyectos->the_post();
                        ?>

                        <div class="card">
                            <div class="card-body">
                                <h6>
                                    <span>
                                        <?php $categories = get_the_category();
                                        if ( ! empty( $categories ) ) {
                                        echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                        } ?>
                                    </span>
                                </h6>
                                <h1 class="pt-3"><a href='<?php the_permalink(); ?>' class="text-dark"><?php the_title(); ?></a></h1>
                                <?php the_excerpt(); ?>
                            </div>                     
                        </div>
                        
                        <?php endwhile; endif; wp_reset_query(); ?>

                    </div>
                
                </div> <!--.col-->
            
            </div> <!--.row-->

            <h1 class="secciones pt-5">Áreas de Trabajo</h1>

            <div class="row pt-0" id="areas-trabajo">
                
                <div class="col-12">

                    <a href="/areas-de-trabajo/">
                        <div class="row py-5 border-bottom">
                            <div class="col-sm-10 col-8 align-self-center">
                                <h1 class="">Acompañamiento</h1>
                            </div>
                            <div class="col-sm-2 col-4 align-self-center">
                                <h2 class="">Saber más</h2>
                            </div>
                        </div>
                    </a>

                    <a href="/areas-de-trabajo/">
                        <div class="row py-5 border-bottom">
                            <div class="col-sm-10 col-8 align-self-center">
                                <h1 class="">Redes</h1>
                            </div>
                            <div class="col-sm-2 col-4 align-self-center">
                                <h2 class="">Saber más</h2>
                            </div>
                        </div>
                    </a>

                    <a href="/areas-de-trabajo/">
                        <div class="row py-5 border-bottom">
                            <div class="col-sm-10 col-8 align-self-center">
                                <h1 class="">Investig-acción</h1>
                            </div>
                            <div class="col-sm-2 col-4 align-self-center">
                                <h2 class="">Saber más</h2>
                            </div>
                        </div>
                    </a>

                </div>
            </div>
            
            <div class="row pt-5" id="ejes">
                <div class="col-12 pb-3">
                    <h1 class="secciones">Ejes Temáticos</h1>
                </div>
                <?php dynamic_sidebar( 'portada_inicio' ); ?>
            </div>
            
            <!-- <div class="row pt-5" id="areas">
                <div class="col-12">
                    <h1 class="secciones">Áreas de Trabajo</h1>
                </div>
            </div> -->

        </div> <!--.container-->

    </div>

<?php get_footer(); ?>
